import java.util.*;

class HNode {
    int data;
    HNode left;
    HNode right;

    public HNode(int data) {
        this.data = data;
        left = null;
        right = null;
    }

    @Override
    public String toString() {
        return data + "";
    }
}

public class T2 {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        //Random sc=new Random();
        int n = sc.nextInt();
        PriorityQueue<HNode> first = new PriorityQueue<>(n, new Comparator<HNode>() {
            @Override
            public int compare(HNode o1, HNode o2) {
                return  (o2.data - o1.data);
            }
        });
        PriorityQueue<HNode> last
                = new PriorityQueue<>(n, new Comparator<HNode>() {
            @Override
            public int compare(HNode o1, HNode o2) {
                return  (o1.data - o2.data);
            }
        });
        int numbers = 0;
        for (int i = 0; i < n; i++) {
            int d = sc.nextInt();
            if (d == 1) {
                numbers++;
                int num = sc.nextInt();
                if (numbers % 3 == 0) {
                    first.add(new HNode(num));
                    last.add(new HNode(first.poll().data));
                } else {
                    last.add(new HNode(num));
                    first.add(new HNode(last.poll().data));
                }
            }
            //System.out.println(Arrays.toString(first.toArray())+":"+Arrays.toString(last.toArray()));
            else if (d == 2) {
                if (last.isEmpty())
                    System.out.println("No reviews yet");
                else {
                    System.out.println(last.peek().data);
                }
            }
        }
    }

}
/*
11 1 1 1 7 2 1 9 2 1 21 1 8 1 5 2 1 10 2
//1 5 7 8 9 21
 */