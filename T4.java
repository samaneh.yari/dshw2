import java.util.PriorityQueue;
import java.util.Scanner;
import java.util.Comparator;


class Node {
    long data;
    Node left;
    Node right;
}

class MyComparator implements Comparator<Node> {
    public int compare(Node x, Node y) {

        return (int) (x.data - y.data);
    }
}

public class T4 {


    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        int n = sc.nextInt();
        long[] charfreq = new long[n];
        long sum=0;
        for (int i = 0; i < n; i++) {
            charfreq[i] = sc.nextLong();
        }

        PriorityQueue<Node> q
                = new PriorityQueue<>(n, new MyComparator());

        for (int i = 0; i < n; i++) {
            Node newNode = new Node();
            newNode.data = charfreq[i];
            newNode.left = null;
            newNode.right = null;
            q.add(newNode);
        }

        Node root = null;

        while (q.size() > 1) {
            Node x = q.peek();
            q.poll();
            Node y = q.peek();
            q.poll();
            Node f = new Node();
            f.data = x.data + y.data;
            sum+=f.data;
            f.left = x;
            f.right = y;
            root = f;
            q.add(f);
        }
        System.out.println(sum+root.data);
    }
}