import java.util.ArrayList;
import java.util.HashSet;
import java.util.Scanner;

public class T1 {

    static ArrayList<Integer>[] graph;

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        graph = new ArrayList[n];
        for (int i = 0; i < n; i++)
            graph[i] = new ArrayList<>();
        for (int i = 0; i < n - 1; i++) {
            int src = sc.nextInt() - 1;
            int dis = sc.nextInt() - 1;
            graph[src].add(dis);
            graph[dis].add(src);
        }
        HashSet<Integer> visited = new HashSet<>();
        DFS(0, visited,0);
        visited = new HashSet<>();
        int maxId=maxDisID;
        maxDis=-1;
        maxDisID=-1;
        DFS(maxId, visited,0);
        System.out.println(maxDis);
    }

    static int maxDis=-1;
    static int maxDisID=-1;
    private static void DFS(int src, HashSet<Integer> visited,int dis) {
        if(maxDis<dis)
        {
            maxDis=dis;
            maxDisID=src;
        }
        if (visited.size() == 0)
            visited.add(src);
        for (Integer temp : graph[src]) {
            if (!visited.contains(temp)) {
                visited.add(temp);
                DFS(temp, visited,dis+1);
            }
        }
    }
}