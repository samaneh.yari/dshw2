import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;

public class T3 {
    static class Coor {
        int x, y;

        public Coor(int x, int y) {
            this.x = x;
            this.y = y;
        }


    }

    public static void main(String[] args) {
        int m, n, k, fnumber = 0, map[][], time[][];
        Scanner scanner = new Scanner(System.in);
        String string;
        n = scanner.nextInt();
        m = scanner.nextInt();
        k = scanner.nextInt();
        scanner.nextLine();
        map = new int[n][m];
        time = new int[n][m];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                time[i][j] = -1;
            }
        }
        Coor t = null, s = null;
        Queue<Coor> fires = new LinkedList<>();

        //getting input
        for (int i = 0; i < n; i++) {
            string = scanner.nextLine();
            for (int j = 0; j < m; j++) {
                if (string.charAt(j) == 'f') {
                    map[i][j] = 1;
                    time[i][j] = 0;
                    fnumber++;
                    fires.add(new Coor(i, j));
                } else if (string.charAt(j) == 't') {
                    map[i][j] = 2;
                    t = new Coor(i, j);
                } else if (string.charAt(j) == 's') {
                    map[i][j] = 3;
                    s = new Coor(i, j);
                }
            }
        }

        if (fires.isEmpty())
            findgreedy(s,t);
        else
            while (fnumber != m * n) {
                Coor c = fires.remove();
                for (int i = -1; i < 2; i++) {
                    for (int j = -1; j < 2; j++) {
                        if (i == 0 && j == 0)
                            continue;
                        try {
                            if (time[c.x + i][c.y + j] == -1) {
                                time[c.x + i][c.y + j] = time[c.x][c.y] + k;
                                fires.add(new Coor(c.x + i, c.y + j));
                                fnumber++;
                            }
                        } catch (Exception e) {

                        }
                    }
                }

            }

//        for (int i = 0; i < n; i++) {
//            for (int j = 0; j < m; j++)
//                System.out.print(time[i][j]);
//            System.out.println();
//        }

        boolean visit[][] = new boolean[n][m];

        nodes.add(new Node(s, 0));
        visit[s.x][s.y] = true;
        while (!nodes.isEmpty()) {
            bfs(t, time, visit);
        }
        if (ans != 99999999)
            System.out.println(ans);
        else
            System.out.println("Impossible");

    }

    private static void findgreedy(Coor s,Coor t) {
        ans=Math.abs(s.x-t.x)+Math.abs(s.y-t.y);
    }

    static int ans = 99999999;
    static Queue<Node> nodes = new LinkedList<>();

    static class Node {
        Coor coor;
        int timer;

        public Node(Coor coor, int timer) {
            this.coor = coor;
            this.timer = timer;
        }

    }

    static void bfs(Coor s, int time[][], boolean visit[][]) {
        Node n = nodes.remove();
        //System.out.println(n.coor.x+":"+n.coor.y);
        if (n.coor.x == s.x && n.coor.y == s.y) {
            if (n.timer < ans) {
                ans = n.timer;
            }
            return;
        }

        try {
            if (time[n.coor.x + 1][n.coor.y] > n.timer + 1 && !visit[n.coor.x + 1][n.coor.y]) {
                visit[n.coor.x + 1][n.coor.y] = true;
                nodes.add(new Node(new Coor(n.coor.x + 1, n.coor.y), n.timer + 1));

            }
        } catch (Exception e) {

        }
        try {
            if (time[n.coor.x - 1][n.coor.y] > n.timer + 1 && !visit[n.coor.x - 1][n.coor.y]) {
                visit[n.coor.x - 1][n.coor.y] = true;
                nodes.add(new Node(new Coor(n.coor.x - 1, n.coor.y), n.timer + 1));
            }
        } catch (Exception e) {

        }
        try {
            if (time[n.coor.x][n.coor.y + 1] > n.timer + 1 && !visit[n.coor.x][n.coor.y + 1]) {
                visit[n.coor.x][n.coor.y + 1] = true;
                nodes.add(new Node(new Coor(n.coor.x, n.coor.y + 1), n.timer + 1));
            }
        } catch (Exception e) {

        }
        try {
            if (time[n.coor.x][n.coor.y - 1] > n.timer + 1 && !visit[n.coor.x][n.coor.y - 1]) {
                visit[n.coor.x][n.coor.y - 1] = true;
                nodes.add(new Node(new Coor(n.coor.x, n.coor.y - 1), n.timer + 1));
            }
        } catch (Exception e) {

        }
    }


}
/*
7 7 2
f------
-f---f-
----f--
-------
------f
---s---
t----f-
3 4 1
t--f
--s-
----
 */